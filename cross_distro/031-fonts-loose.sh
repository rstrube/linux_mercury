#!/bin/sh
# Script: Loose Font Installation (in ~/.fonts) [Generic]
# Author: Robert Strube
# Date: 2020-08-10

# This script copies fonts to your $HOME
# 1. TTFs from Microsoft Windows 10

mkdir -p $HOME/.fonts/ttf-ms-win10
tar -xf ../fonts/ttf-ms-win10/ttf-ms-win10.tar.xz -C $HOME/.fonts/ttf-ms-win10

# This rebuilds the font-cache, taking into account any changes
sudo fc-cache -r -v
