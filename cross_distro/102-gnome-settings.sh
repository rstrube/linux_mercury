#!/bin/sh
# Script: Gnome Settings [Generic]
# Author: Robert Strube
# Date: 2020-08-90

# Disable Gnome animations
gsettings set org.gnome.desktop.interface enable-animations false

# Show battery percentage in Gnome topbar
gsettings set org.gnome.desktop.interface show-battery-percentage true

# Show weekday in Gnome topbar
gsettings set org.gnome.desktop.interface clock-show-weekday true

# Set a custom keybinding to launch terminal with Super+T
gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/']"

# Keybind 0
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding "<Super>t"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command "gnome-terminal"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name "Launch terminal"
