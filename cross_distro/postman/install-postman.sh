#!/bin/bash

echo "Downloading Postman ..."
wget -q https://dl.pstmn.io/download/latest/linux?arch=64 -O postman.tar.gz
tar -xzf postman.tar.gz
rm postman.tar.gz

echo "Installing to opt..."
if [ -d "/opt/postman" ];then
    sudo rm -rf /opt/postman
fi
sudo mv ./Postman /opt/postman

echo "Creating symbolic link..."
if [ -L "/usr/bin/postman" ];then
    sudo rm -f /usr/bin/postman
fi
sudo ln -s /opt/postman/Postman /usr/bin/postman

echo "Creating .desktop file..."
if [ -e "/usr/share/applications/postman.desktop" ];then
    sudo rm /usr/share/applications/postman.desktop
fi
sudo cp ./postman.desktop /usr/share/applications/postman.desktop

echo "Installation completed successfully."
echo "You can use Postman!"
