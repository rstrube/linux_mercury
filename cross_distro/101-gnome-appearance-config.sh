#!/bin/sh
# Script: Gnome Appearance Configuration [Generic]
# Author: Robert Strube
# Date: 2020-08-10

# Reference

# To see a list of schemas:
# gsettings list-schemas | sort

# To see keys for a schema:
# gsettings list-keys org.gnome.desktop.interface

# To set a key:
# gsettings set {key} {value}

# Set icon theme to Papirus Dark
# Note for Manjaro Gnome the Papirus icon theme is already installed
gsettings set org.gnome.desktop.interface icon-theme "Papirus-Dark"

# Get GTK theme to Dracula (which was locally installed in ~/.themes)
gsettings set org.gnome.desktop.interface gtk-theme "Dracula"
gsettings set org.gnome.desktop.wm.preferences theme "Dracula"

# Set Gnome fonts
gsettings set org.gnome.desktop.interface font-name "Roboto 10"
gsettings set org.gnome.desktop.interface document-font-name "Roboto Slab 11"
gsettings set org.gnome.desktop.interface monospace-font-name "JetBrains Mono 11"
gsettings set org.gnome.desktop.wm.preferences titlebar-font "Roboto Medium 10"

# This will list out all the folder color options available
# papirus-folders -l --theme Papirus-Dark

# Change folder color to "grey" which works really well with the Dracular GTK theme
papirus-folders -C grey --theme Papirus-Dark

# Set Gnome Terminal Profile for Dracula Theme
# 1. Reset all settings
dconf reset -f /org/gnome/terminal/

# 2. Load settings from file
dconf load /org/gnome/terminal/ < ../cross_distro/themes/gnome-terminal/gnome_terminal_settings_dracula.txt

# Reference: dump Gnome Terminal configuration to txt file
# dconf dump /org/gnome/terminal/ > gnome_terminal_settings_dracula.txt
