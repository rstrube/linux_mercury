#!/bin/sh
# Script: Install Flatpak Applications [Generic]
# Author: Robert Strube
# Date: 2020-08-13

# Flatpak Applications:
# slack: destkop application for connecting to the slack chat platform
# darktable: an extremely powerful RAW photo editor
# meld: a code merging and diff tool
# transmission: a fantastic bittorrent client
# tauon music box: a advanced fast music library manager and player
# calbire: an ebook manager
# teams: desktop teams appliction for connecting to the teams chat platform
# lollypop: a GTK music player and manager
# postman: a tools for making HTTP requests (REST API testing)
# xournalpp: a PDF annotation tool

flatpak install -y flathub com.slack.Slack
flatpak install -y flathub org.darktable.Darktable
flatpak install -y flathub org.gnome.meld
flatpak install -y flathub com.transmissionbt.Transmission
flatpak install -y flathub com.github.taiko2k.tauonmb
flatpak install -y flathub com.calibre_ebook.calibre
flatpak install -y flathub flathub org.gnome.GTG
flatpak install -y flathub com.github.xournalpp.xournalpp

# Replaced flatpak version of Teams with "native" version because of CPU issues
# flatpak install -y flathub com.microsoft.Teams

# Replaced with Tauon Music Box (more advanced music player)
# flatpak install -y flathub org.gnome.Lollypop

# Flatpak version always seems very far behind, replacing with manually downloaded version
# flatpak install -y flathub com.getpostman.Postman

# Reference:
# To list out all available themes
# flatpak remote-ls flathub | grep org.gtk.Gtk3theme

# You can setup GTK themes for you flatpak applications
# flatpak install -y flathub org.gtk.Gtk3theme.Adwaita-dark

flatpak install -y flathub org.gtk.Gtk3theme.High-Sierra-Dark

# Force org.gnome.GTG to use specific flatpak theme
flatpak override --user --env=GTK_THEME=High-Sierra-Dark org.gnome.GTG

# Install the local GTK theme as the flatpak theme
# Please see https://github.com/refi64/pakitheme/
../cross_distro/pakitheme/pakitheme install-user
