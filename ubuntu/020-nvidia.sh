#!/bin/sh
# Script: Nvidia Drivers / Vulkan
# Author: Robert Strube
# Date: 2020-03-28

# The proprietary Nvidia drivers currently provides OpenGL and Vulkan support for Nvidia GPUs

# The Ubuntu "graphics drivers team" currently provides a PPA for the most up-to-date Nvidia drivers
# See https://launchpad.net/~graphics-drivers/+archive/ubuntu/ppa for more information
sudo add-apt-repository -y ppa:graphics-drivers/ppa

# The most current version is 440.64
sudo apt install -y nvidia-driver-440 libvulkan1 libvulkan1:i386 vulkan-utils

# The PPA also updates many other packages that come preinstalled from the Ubuntu repositories
sudo apt upgrade -y

# Beacuse of the way the driver packages are defined (e.g. nvidia-driver-435, nvidia-driver-440)
# its important to remove any older drivers that might be installed
sudo apt autoremove -y

