#!/bin/sh
# Script: KVM/QEMU Native Virtualization [Ubuntu]
# Author: Robert Strube
# Date: 2020-03-28

# KVM supports virtualization of other OSes directly in the linux kernel

# Installs all the neccessary packages to support running / managing KVM virtual machines
sudo apt install -y qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils virt-manager

# Add user to the correct groups
sudo usermod -a -G libvirt $USER
sudo usermod -a -G libvirt-qemu $USER

