#!/bin/sh
# Script: Gnome Appearance Resources [Ubuntu]
# Author: Robert Strube
# Date: 2020-08-10

# Install the Papirus icon theme (high coverage icon theme) and Papirus Folders utility
sudo add-apt-repository -y ppa:papirus/papirus
sudo apt-get install -y papirus-icon-theme papirus-folders

# Install Dracula GTK theme to ~/.themes
mkdir -p $HOME/.themes
cp -r ../cross_distro/themes/gtk/Dracula $HOME/.themes
