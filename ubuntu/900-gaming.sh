#!/bin/sh
# Script: Gaming [Ubuntu]
# Author: Robert Strube
# Date: 2020-03-28

# Steam
sudo apt install -y steam steam-devices

# Some libraries that might be neccessary for some native Linux games
# Note: this might not be neccessary anymore, leaving here for reference purposes
# sudo apt install -y libc++1 libc++abi1 libgconf-2-4

# Valve can't ship Proton (their tweaked version of Wine) with the FAudio library with native ffmpeg support
# Its possible to build the FAudio library with ffmpeg support and then overwrite the existing *.so files
# These are the packages neccessary to build FAudio with ffmpeg support
# See https://github.com/FNA-XNA/FAudio/wiki/FAudio-for-Proton for more information
# sudo apt install -y build-essential cmake cmake-curses-gui ffmpeg libavcodec-dev libavutil-dev libsdl2-dev

sudo apt-get install -y --install-recommends wine-development winetricks
###############################################################################

# Lutris
sudo add-apt-repository -y ppa:lutris-team/lutris
sudo apt-get install -y lutris


