#!/bin/sh
# Script: Gnome RAW Image Thumbnail Support [Ubuntu]
# Author: Robert Strube
# Date: 2020-03-28

# By default Nautilus (the Gnome file manager) will not show preview images for RAW images (used on higher end digital cameras)
# this installs ufraw-batch, and then setups some scripts that Nautilus will run to create thumbail previews for RAW images

sudo apt install -y ufraw-batch

# Setups up the appropriate configuration for the Nautilus thubmnailer
echo "[Thumbnailer Entry]" | sudo tee /usr/share/thumbnailers/raw.thumbnailer
echo "TryExec=ufraw-batch" | sudo tee -a /usr/share/thumbnailers/raw.thumbnailer
echo "Exec=ufraw-batch --silent --size %s --out-type=png --noexif --output=%o --overwrite --embedded-image %i" | sudo tee -a /usr/share/thumbnailers/raw.thumbnailer
echo "MimeType=image/x-canon-cr2;image/x-canon-crw;image/x-minolta-mrw;image/x-nikon-nef;image/x-pentax-pef;image/x-panasonic-rw2;image/x-samsung-srw;image/x-olympus-orf" | sudo tee -a /usr/share/thumbnailers/raw.thumbnailer

# Kill Nautilus
nautilus -q

# Remove the old Nautilus cache so that it's rebuilt
rm -rf $HOME/.cache/thumbnails/* $HOME/.thumbnails/*

