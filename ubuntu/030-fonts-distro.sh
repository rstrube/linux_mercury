#!/bin/sh
# Script: Font Installation [Ubuntu]
# Author: Robert Strube
# Date: 2020-08-10

# For reference the Pop_OS font configuration is:
# Window Titles: Fira Sans SemiBold 10
# Interface: Fira Sans Book 10
# Documents: Roboto Slab Regular 11
# Monospace: Fira Mono Regular 11

# Install the following fonts:
# Fira Code: https://github.com/tonsky/FiraCode
# Fira Code has been patched to support "Powerline" symbols that oh-my-zsh requires
# Roboto: default font for Google's Android OS
# Jetbrains Mono: a fantastic monospace font
sudo apt install -y fonts-roboto fonts-roboto-slab fonts-firacode

# Install JetBrains Mono from loose fonts
mkdir -p $HOME/.fonts
cp -r ../fonts/_fonts/JetBrainsMono-2.001 $HOME/.fonts/

# This rebuilds the font-cache, taking into account any changes
sudo fc-cache -r -v
