#!/bin/sh
# Script: Base Installation
# Author: Robert Strube
# Date: 2020-04-30

GIT_USERNAME="Robert Strube"
GIT_EMAIL="rstrube@gmail.com"

# Update system
sudo apt update
sudo apt upgrade -y && sudo apt dist-upgrade -y
sudo apt autoremove -y

# Packages:
# dkms: dynamic kernel module system - neccessary to support out of tree kernel modules (VirtualBox, Nvidia Driver, etc.)
# exfat-fuse and exfat-utils: neccessary to support exFAT which is used on large SD cards (digital cameras etc)
# git: unbiquitous source code management system
# htop: htop is a console based resource monitoring tool
# jmtpfs: neccesary to mount android phone storage when plugging in phone via USB
# neovim: neovim is a more modern vim alternative that is completely compatible with standard vim 
sudo apt install -y dkms exfat-fuse exfat-utils git htop jmtpfs neovim

# Additional Packages:
# apt-file: supports some advanced file search operations across apt package repositories
# sudo apt install apt-file

# Set git configuration
git config --global user.name $GIT_USERNAME
git config --global user.email $GIT_EMAIL


