#!/bin/sh
# Script: Flatpak Base Installation [Ubuntu]
# Author: Robert Strube
# Date: 2020-04-04

# Packages:
# flatpak: allows users to install containerized applications.  Check out https://flathub.org for more information
sudo apt install -y flatpak

# This sets up flathub as a flatpak remote
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
