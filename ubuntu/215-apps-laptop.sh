#!/bin/sh
# Script: Install Laptop Applications [Ubuntu]
# Author: Robert Strube
# Date: 2020-03-28

# Sets up a PPA / installs touchpad-indicator system tray tool
# This is very useful to define rules around enabling / disabling your latop touchpag
sudo add-apt-repository -y ppa:atareao/atareao
sudo apt install -y touchpad-indicator

