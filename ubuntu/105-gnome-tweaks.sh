#!/bin/sh
# Script: Gnome Tweaking Support [Ubuntu]
# Author: Robert Strube
# Date: 2020-04-30

# This installs the gnome tweak tool as well as a firefox extension for installing gnome extensions
# through https://extensions.gnome.org
sudo apt install -y chrome-gnome-shell gnome-tweaks

