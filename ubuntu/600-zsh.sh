#!/bin/sh
# Script: Zsh and Oh-My-Zsh [Ubuntu]
# Author: Robert Strube
# Date: 2020-03-28

# zsh provides many advantages over bash as a shell
# Oh-My-Zsh provides some enhancements and tools, especially when dealing with git on the command line
# See https://ohmyz.sh/ and https://github.com/ohmyzsh/ohmyzsh/wiki/Themes

# Install zsh
sudo apt install -y zsh

# Install Oh-My-Zsh
sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"

# Copy .zshrc into place
cp -v ../dotfiles/_zshrc-oh-my-zsh $HOME/.zshrc

