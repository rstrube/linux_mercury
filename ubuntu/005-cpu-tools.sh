#!/bin/sh
# Script: CPU Tools and Utilities
# Author: Robert Strube
# Date: 2020-04-30

# Packages:
#
# For more information on undervolting, changing TDP, max boost time, etc. check out: https://gist.github.com/Mnkai/5a8edd34bd949199224b33bd90b8c3d4
#
# msr-tools: neccessary to support changing Intel CPU voltage, typically used to undervolt on laptops
sudo apt install -y msr-tools

# Additional Packages:
# devmem2: neccessary to directly write to memory to support changing TDP (thermal limits) and max boost time for Intel CPUs
# linux-tools-generic: contains the turbostat program which is used to validate TDP and max boost time changes
# sudo apt install -y devmem2 linux-tools-generic 


