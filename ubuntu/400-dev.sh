#!/bin/sh
# Script: Development Tools [Ubuntu]
# Author: Robert Strube
# Date: 2020-04-30

# This installs some foundational development compliers, tools, and applications

# Packages:
# build-essential: Ubuntu's metapackage for a bunch of development compilers and tools
# flex: a tool for lexing C/C++ code, neccessary for kernel compilation
# bison: a tool for parsing code, neccessary for kernel compilation
# cmake cmake-curses-gui: an autotools replacement, some programs require it in order to configure before build
# apache2-utils: includes some tools for simulating web-requests.  Useful for testing performance of site under load
sudo apt install -y build-essential flex bison cmake cmake-curses-gui apache2-utils

# Visual Studio Code
###############################################################################
if [ ! -e /etc/apt/trusted.gpg.d/microsoft.gpg ]; then
	wget -qO - https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
	sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
fi
if [ ! -e /etc/apt/sources.list.d/vscode.list ]; then
	echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" | sudo tee /etc/apt/sources.list.d/vscode.list
fi
sudo apt update
sudo apt install -y code
###############################################################################

# .NET Core
###############################################################################
if [ ! -e /etc/apt/trusted.gpg.d/microsoft.gpg ]; then
	wget -qO - https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
	sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
fi
if [ ! -e /etc/apt/sources.list.d/dotnet.list ]; then
	echo "deb [arch=amd64] https://packages.microsoft.com/ubuntu/19.04/prod disco main" | sudo tee /etc/apt/sources.list.d/dotnet.list
fi
sudo apt-get update
sudo apt-get install -y apt-transport-https
sudo apt-get update
sudo apt-get install -y dotnet-sdk-3.1
###############################################################################

# Postman
###############################################################################
cd ../cross_distro/postman
./install-postman.sh
###############################################################################
