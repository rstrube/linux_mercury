#!/bin/sh
# Script: Install Core Applications [Ubuntu]
# Author: Robert Strube
# Date: 2020-03-28

# Packages:
# google-chrome-stable: the ubiquitous web browser
################################################################################
if [ ! -e /etc/apt/trusted.gpg.d/google.gpg ]; then
	wget -qO - https://dl.google.com/linux/linux_signing_key.pub | gpg --dearmor > google.gpg
	sudo mv google.gpg /etc/apt/trusted.gpg.d/google.gpg
fi

if [ ! -e /etc/apt/sources.list.d/google-chrome.list ]; then
	echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" | sudo tee /etc/apt/sources.list.d/google-chrome.list
fi

sudo apt update
sudo apt install -y google-chrome-stable
################################################################################

# Packages:
# flameshot libqt5svg5: flameshot is an amazing screenshot utility, the library is to work around some icon issues within the tool
# neofetch: a console application for displaying your system information (newer version of screenfetch)
# pavucontrol: a dedicated application for managing pulseaudio sinks.  Can be useful above and beyond the built in "sounds" setting tool that comes with Gnome
# s-tui: an incredible CPU monitor which provides frequency and temperature information 
sudo apt install -y flameshot libqt5svg5 neofetch pavucontrol s-tui

# Packages:
# ubuntu-restricted-extras ubuntu-restricted-addons: meta packages that install codecs and MS fonts
sudo apt install -y ubuntu-restricted-extras ubuntu-restricted-addons

# Because new fonts were installed, we need to rebuild the font cache
sudo fc-cache -f -v

