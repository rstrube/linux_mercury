#!/bin/sh
# Script: Mesa Drivers / Vulkan
# Author: Robert Strube
# Date: 2020-03-28

# Mesa currently provides OpenGL and Vulkan support for Intel and AMD GPUs

# This sets up a Mesa PPA so we can install a more up-to-date Mesa
# The Mesa PPA is from a Valve developer [https://launchpad.net/~kisak/+archive/ubuntu/kisak-mesa]
###############################################################################
sudo add-apt-repository -y ppa:kisak/kisak-mesa

# Note that the PPA will update many existing packages from Ubuntu repostories,
# this is why after adding the PPA we need to run the following:
sudo apt upgrade -y && sudo apt dist-upgrade -y
###############################################################################

# Install packages for VA-API (faster video encoding / decoding with Intel iGPUs)
# Note: some of these packages might already be installed, so they will be skipped
sudo apt install -y va-driver-all vainfo gstreamer1.0-vaapi

# Steam requires i386 packages to function correctly
sudo apt install -y libgl1-mesa-dri:i386 libgl1-mesa-glx:i386 mesa-vulkan-drivers mesa-vulkan-drivers:i386 mesa-utils vulkan-utils

