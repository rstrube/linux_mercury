#!/bin/sh
# Script: Intel VAAPI Support [Manjaro]
# Author: Robert Strube
# Date: 2020-08-04

# Install VAAPI support for Intel iGPUs
pamac install manjaro-vaapi gstreamer-vaapi libva-utils

# currently GStreamer doesn't support the Intel iHD driver by befault
# setting this environment variable enables support for experimental VAAPI backends
echo "GST_VAAPI_ALL_DRIVERS=1" | sudo tee -a /etc/environment

# Reference:
# To check for driver support:
# vainfo
# You should see output like:
# ~ >>> vainfo                                                                                                                               
# vainfo: VA-API version: 1.8 (libva 2.7.1)
# vainfo: Driver version: Intel iHD driver for Intel(R) Gen Graphics - 20.1.1 ()
# vainfo: Supported profile and entrypoints
#       VAProfileNone                   :	VAEntrypointVideoProc
#       VAProfileNone                   :	VAEntrypointStats
#       VAProfileMPEG2Simple            :	VAEntrypointVLD
#       VAProfileMPEG2Simple            :	VAEntrypointEncSlice
#       VAProfileMPEG2Main              :	VAEntrypointVLD
#       VAProfileMPEG2Main              :	VAEntrypointEncSlice
#       VAProfileH264Main               :	VAEntrypointVLD
#       VAProfileH264Main               :	VAEntrypointEncSlice
#       VAProfileH264Main               :	VAEntrypointFEI
#       VAProfileH264Main               :	VAEntrypointEncSliceLP
#       VAProfileH264High               :	VAEntrypointVLD
#       VAProfileH264High               :	VAEntrypointEncSlice
#       VAProfileH264High               :	VAEntrypointFEI
#       VAProfileH264High               :	VAEntrypointEncSliceLP
#       VAProfileVC1Simple              :	VAEntrypointVLD
#       VAProfileVC1Main                :	VAEntrypointVLD
#       VAProfileVC1Advanced            :	VAEntrypointVLD
#       VAProfileJPEGBaseline           :	VAEntrypointVLD
#       VAProfileJPEGBaseline           :	VAEntrypointEncPicture
#       VAProfileH264ConstrainedBaseline:	VAEntrypointVLD
#       VAProfileH264ConstrainedBaseline:	VAEntrypointEncSlice
#       VAProfileH264ConstrainedBaseline:	VAEntrypointFEI
#       VAProfileH264ConstrainedBaseline:	VAEntrypointEncSliceLP
#       VAProfileVP8Version0_3          :	VAEntrypointVLD
#       VAProfileVP8Version0_3          :	VAEntrypointEncSlice
#       VAProfileHEVCMain               :	VAEntrypointVLD
#       VAProfileHEVCMain               :	VAEntrypointEncSlice
#       VAProfileHEVCMain               :	VAEntrypointFEI
#       VAProfileHEVCMain10             :	VAEntrypointVLD
#       VAProfileHEVCMain10             :	VAEntrypointEncSlice
#       VAProfileVP9Profile0            :	VAEntrypointVLD
#       VAProfileVP9Profile2            :	VAEntrypointVLD

# To check for GStreamer support
# gst-inspect-1.0 vaapi
# ~ >>> gst-inspect-1.0 vaapi                                                                                                                
# Plugin Details:
#   Name                     vaapi
#   Description              VA-API based elements
#   Filename                 /usr/lib/gstreamer-1.0/libgstvaapi.so
#   Version                  1.16.2
#   License                  LGPL
#   Source module            gstreamer-vaapi
#   Binary package           gstreamer-vaapi
#   Origin URL               http://bugzilla.gnome.org/enter_bug.cgi?product=GStreamer

#   vaapijpegdec: VA-API JPEG decoder
#   vaapimpeg2dec: VA-API MPEG2 decoder
#   vaapih264dec: VA-API H264 decoder
#   vaapivc1dec: VA-API VC1 decoder
#   vaapivp8dec: VA-API VP8 decoder
#   vaapivp9dec: VA-API VP9 decoder
#   vaapih265dec: VA-API H265 decoder
#   vaapipostproc: VA-API video postprocessing
#   vaapidecodebin: VA-API Decode Bin
#   vaapisink: VA-API sink
#   vaapimpeg2enc: VA-API MPEG-2 encoder
#   vaapih265enc: VA-API H265 encoder
#   vaapivp8enc: VA-API VP8 encoder
#   vaapijpegenc: VA-API JPEG encoder
#   vaapih264enc: VA-API H264 encoder
#   vaapih264feienc: VA-API H264 FEI Advanced encoder (Experimental)
