#!/bin/sh
# Script: Mesa Drivers & Utilities [Manjaro]
# Author: Robert Strube
# Date: 2020-08-04

# vulkan-tools: a suite of vulkan related tools and utilities including vulkaninfo
pamac install vulkan-tools

