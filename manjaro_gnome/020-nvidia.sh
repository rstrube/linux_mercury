#!/bin/sh
# Script: Nvidia Drivers / Vulkan [Manjaro]
# Author: Robert Strube
# Date: 2020-08-04

mhwd -i pci video-nvidia-440xx
pamac install vulkan-tools

