#!/bin/sh
# Script: Base Installation [Manjaro]
# Author: Robert Strube
# Date: 2020-07-30

GIT_USERNAME="Robert Strube"
GIT_EMAIL="rstrube@gmail.com"

# Update system
pamac checkupdates -a
pamac update -a
pamac install linux-latest linux-latest-headers

# Setup pamac to use AUR
sudo sed --in-place "s/#EnableAUR/EnableAUR/" "/etc/pamac.conf"
sudo sed --in-place "s/#CheckAURUpdates/CheckAURUpdates/" "/etc/pamac.conf"

# Packages:
# exfat-utils: support for exFAT file systems
# git: unbiquitous source code management system
# htop: htop is a console based resource monitoring tool
# neovim: neovim is a more modern vim alternative that is completely compatible with standard vim

pamac install exfat-utils git htop neovim

# Set git configuration
git config --global user.name $GIT_USERNAME
git config --global user.email $GIT_EMAIL
