#!/bin/sh
# Script: Gnome Appearance Resources [Manjaro]
# Author: Robert Strube
# Date: 2020-08-10

# Install Dracula GTK theme to ~/.themes
mkdir -p $HOME/.themes
cp -r ../cross_distro/themes/gtk/Dracula $HOME/.themes

# Install utility to change folder color for Papirus icon theme
pamac install papirus-folders
