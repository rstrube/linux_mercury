#!/bin/sh
# Script: KVM/QEMU Native Virtualization [Manjaro]
# Author: Robert Strube
# Date: 2020-08-09

# KVM supports virtualization of other OSes directly in the linux kernel

# Installs all the neccessary packages to support running / managing KVM virtual machines
# Note edk2-ovmf supports VMs with UEFI instead of traditional BIOS
pamac install qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat ebtables edk2-ovmf

sudo systemctl enable libvirtd.service
sudo systemctl start libvirtd.service

# Tweak some permissions for libvirtd to allow non-root users to interact with KVM virtual machines
sudo sed --in-place "s/#unix_sock_group/unix_sock_group/" "/etc/libvirt/libvirtd.conf"
sudo sed --in-place "s/#unix_sock_ro_perms/unix_sock_ro_perms/" "/etc/libvirt/libvirtd.conf"

# Add user to the correct groups
sudo usermod -a -G libvirt $USER
sudo usermod -a -G libvirt-qemu $USER

sudo systemctl restart libvirtd.service

# Set up a default network, and set it to autostart
sudo virsh net-define --file ./kvm-qemu/default.xml
sudo virsh net-start default
sudo virsh net-autostart --network default
