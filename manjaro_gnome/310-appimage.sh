#!/bin/sh
# Script: AppImage Helpers and Utilities [Manjaro]
# Author: Robert Strube
# Date: 2020-08-10

# Packages:
# appimagelauncher: a helper utility to integrate app images into the DE's launcher
pamac install appimagelauncher
