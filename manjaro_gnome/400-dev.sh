#!/bin/sh
# Script: Development Tools [Manjaro]
# Author: Robert Strube
# Date: 2020-08-11

# This installs some foundational development compliers, tools, and applications

# Packages:
# base-devel: Manjaro's meta package for development tools (use pacman -Sg base-devel to see packages)
# cmake: an autotools replacement, some programs require it in order to configure before build
# code: visual studio code
# dotnet-sdk: .NET core SDK
# postman: rest API testing application and toolkit
# git-credential-manager: an application that provides support for authenicating with Azure Devops with needing to use PATs
pamac install base-devel cmake code dotnet-sdk postman git-credential-manager
