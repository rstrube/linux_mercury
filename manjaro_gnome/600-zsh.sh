#!/bin/sh
# Script: Zsh [Manjaro]
# Author: Robert Strube
# Date: 2020-08-04

# Manjaro Gnome comes with Zsh installed by default

# Copy .zshrc into place
cp -v ../dotfiles/_zshrc-manjaro-gnome $HOME/.zshrc
