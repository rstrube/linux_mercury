#!/bin/sh
# Script: Flatpak Base Installation [Manjaro]
# Author: Robert Strube
# Date: 2020-07-30

# Packages:
# flatpak: allows users to install containerized applications.  Check out https://flathub.org for more information
pamac install xdg-desktop-portal xdg-desktop-portal-gtk flatpak pamac-flatpak-plugin

# This sets up flathub as a flatpak remote
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Setup pamac for Flatpak support
sudo sed --in-place "s/#EnableFlatpak/EnableFlatpak/" "/etc/pamac.conf"
sudo sed --in-place "s/#CheckFlatpakUpdates/CheckFlatpakUpdates/" "/etc/pamac.conf"
