#!/bin/sh
# Script: Install Core Applications [Manjaro]
# Author: Robert Strube
# Date: 2020-08-11

# Packages:
# google-chrome: the ubiquitous web browser
# flameshot: flameshot is an amazing screenshot utility, the library is to work around some icon issues within the tool
# neofetch: a console application for displaying your system information (newer version of screenfetch)
# pavucontrol: a dedicated application for managing pulseaudio sinks.  Can be useful above and beyond the built in "sounds" setting tool that comes with Gnome
# s-tui: an incredible CPU monitor which provides frequency and temperature information 
# a52dec faac faad2 flac jasper lame libdca libdv libmad libmpeg2 libtheora libvorbis libxv opus wavpack x264 xvidcore libdvdcss: essential codes
# vlc: a fantastic video player
# seahorse: provides a GUI for managing your GPG, OpenSSH, and Gnome Keyring keys
pamac install google-chrome flameshot neofetch pavucontrol s-tui a52dec faac faad2 flac jasper lame libdca libdv libmad libmpeg2 libtheora libvorbis libxv opus wavpack x264 xvidcore libdvdcss vlc seahorse
