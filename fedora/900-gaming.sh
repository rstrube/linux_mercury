#!/bin/sh
# Script: Gaming [Fedora]
# Author: Robert Strube
# Date: 2020-03-29

# Note this script requires that the "unofficial" RPMFusion repositories have
# been added to the system

# Steam
sudo dnf install -y steam

# Some libraries that might be neccessary for some native Linux games
# Note: this might not be neccessary anymore, leaving here for reference purposes
# sudo dnf install -y libc++1 libc++abi1 libgconf-2-4


