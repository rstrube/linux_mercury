#!/bin/sh
# Script: Gnome Tweaking Support [Fedora]
# Author: Robert Strube
# Date: 2020-03-28

# This installs the gnome tweak tool, the dedicated gnome extensions app,
# and the browser extension for installing gnome extensions through https://extensions.gnome.org
sudo dnf install -y chrome-gnome-shell gnome-extensions-app gnome-tweaks
