#!/bin/sh
# Script: Intel VAAPI Support [Fedora]
# Author: Robert Strube
# Date: 2020-03-30

# VAAPI is a video acceleration library developed by Intel that will be supported by both Chrome and Firefox to greatly reduce CPU overhead

# See https://wiki.archlinux.org/index.php/Hardware_video_acceleration for more information

sudo dnf install -y libva libva-intel-driver libva-utils

# Reference
# vainfo: tool to validate that VAAPI is working correctly and what codecs will be accelerated


