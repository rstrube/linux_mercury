#!/bin/sh
# Script: Development Tools [Fedora]
# Author: Robert Strube
# Date: 2020-03-28

# This installs some foundational development compliers, tools, and applications
sudo dnf groupinstall -y 'Development Tools'

# Reference, to list out all currently installed RPM keys:
# rpm -qa --qf '%{VERSION}-%{RELEASE} %{SUMMARY}\n' gpg-pubkey*

# Visual Studio Code
###############################################################################
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc

if [ ! -e /etc/yum.repos.d/vscode.repo ]; then
	sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
fi
sudo dnf check-upgrade
sudo dnf install -y code
###############################################################################


