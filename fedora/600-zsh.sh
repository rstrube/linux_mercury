#!/bin/sh
# Script: Zsh and Oh-My-Zsh [Fedora]
# Author: Robert Strube
# Date: 2020-03-29

# zsh provides many advantages over bash as a shell
# Oh-My-Zsh provides some enhancements and tools, especially when dealing with git on the command line
# See https://ohmyz.sh/ and https://github.com/ohmyzsh/ohmyzsh/wiki/Themes

# Packages:
# util-linux-user: includes the chsh tool which lets users change their default shell
# zsh: the package for Z shell
sudo dnf install -y util-linux-user zsh

# Note: the Oh-My-Zsh install script prompts the user to change their shell
# but for reference this is how you do it manually
# chsh -s $(which zsh)

# Install Oh-My-Zsh
sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"

# Copy .zshrc into place
cp -v ../dotfiles/_zshrc-oh-my-zsh $HOME/.zshrc
