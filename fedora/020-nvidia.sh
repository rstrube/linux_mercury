#!/bin/sh
# Script: Nvidia Drivers / Vulkan [Fedora]
# Author: Robert Strube
# Date: 2020-03-29

# Note: this script assumes you have enabled the appropriate RPMFusion repositories (that contain the Nvidia driver)

# The proprietary Nvidia drivers currently provides OpenGL and Vulkan support for Nvidia GPUs

# Fedora uses AKMOD instead of DKMS.  The end result is the same though, when your kernel is updated,
# the Nvidia kernel module will be recompiled
sudo dnf install -y akmod-nvidia vulkan-tools

# If you want to support CUDA, or overvide your power limits you'll need
# this additional package.  It also provides the nvidia-smi tool
sudo dnf install -y xorg-x11-drv-nvidia-cuda


