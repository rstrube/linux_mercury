#!/bin/sh
# Script: Flatpak Base Installation [Fedora]
# Author: Robert Strube
# Date: 2020-04-04

# Setup https://flathub.org as a remote for flatpaks
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
