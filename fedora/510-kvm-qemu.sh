#!/bin/sh
# Script: KVM/QEMU Native Virtualization [Fedora]
# Author: Robert Strube
# Date: 2020-03-28

# KVM supports virtualization of other OSes directly in the linux kernel

# Note see https://fedoramagazine.org/full-virtualization-system-on-fedora-workstation-30/ for additional information

# Install the virtualization package group
# sudo dnf install -y @virtualization

# Uncomment some properties in libvirtd.conf to allow noon root user to work with the libvirt daemon
# in this case any user that is part of the 'libvirt' group can work with the libvirt daemon
sudo sed -i 's/#unix_sock_group = "libvirt"/unix_sock_group = "libvirt"/' /etc/libvirt/libvirtd.conf
sudo sed -i 's/#unix_sock_rw_perms = "0770"/unix_sock_rw_perms = "0770"/' /etc/libvirt/libvirtd.conf

# Enabled the libvirt daemon
sudo systemctl start libvirtd
sudo systemctl enable libvirtd

# Add user to the 'libvirt' group
sudo usermod -a -G libvirt $USER
