#!/bin/sh
# Script: Base Installation [Fedora]
# Author: Robert Strube
# Date: 2020-04-04

GIT_USERNAME="Robert Strube"
GIT_EMAIL="rstrube@gmail.com"

# Upgrade all packages and remove all "leaf" packages before getting started
sudo dnf upgrade 

# For now we won't remove any "leaf" packages
# sudo dnf autoremove 

# Install additional "official" RPMFusion repositories to your /etc/yum.repos.d directory
# These repositories will be *DISABLED* and must be explicitly enabled
# Doing this will allow installation of various additional applications in the software center / command line
# e.g. Chrome, Nvidia drivers, Steam, etc.
# Note: these repositories do *NOT* include additional codecs, if you want those see below
sudo dnf install -y fedora-workstation-repositories

# Install additional "unofficial" RPMFusion repositories to your /etc/yum.repos.d directory from RPMFusion
# these repositories will be *ENABLED* automatically
# Doing this will allow installation of various additional applications in the software center / command line
# but it's primarily used for the installation of MultiMedia codecs
sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm 
sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# Note to list out all repositories enabled and disabled use:
# sudo dnf repolist --all

# Note to list out all enabled repositories use:
# sudo dnf repolist

# Refresh the repositories after adding them above
sudo dnf check-upgrade --refresh

# Packages:
# fuse-exfat: neccessary to support exFAT which is used on large SD cards (digital cameras etc)
# git: unbiquitous source code management system
# htop: htop is a console based resource monitoring tool
# jmtpfs: neccesary to mount android phone storage when plugging in phone via USB
# neovim: neovim is a more modern vim alternative that is completely compatible with standard vim 
sudo dnf install -y fuse-exfat git htop jmtpfs neovim

# Set git configuration
git config --global user.name $GIT_USERNAME
git config --global user.email $GIT_EMAIL


