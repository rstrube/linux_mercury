#!/bin/sh
# Script: CPU Tools and Utilities [Fedora]
# Author: Robert Strube
# Date: 2020-03-28

# Packages:
#
# For more information on undervolting, changing TDP, max boost time, etc. check out: https://gist.github.com/Mnkai/5a8edd34bd949199224b33bd90b8c3d4
#
# msr-tools: neccessary to support changing Intel CPU voltage, typically used to undervolt on laptops
sudo dnf install -y msr-tools

