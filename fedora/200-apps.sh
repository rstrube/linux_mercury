#!/bin/sh
# Script: Install Core Applications [Fedora]
# Author: Robert Strube
# Date: 2020-03-28

# Google Chrome
###############################################################################
# Note this portion of the script requires that the "official" RPMFusion repositories have been installed by
# installing the fedora-workstation-repositories package

# Enable the /etc/yum.repos.d/google-chrome.repo
sudo dnf config-manager --set-enabled google-chrome

# Install
sudo dnf install -y google-chrome-stable

# Applications
###############################################################################
# Packages:
# flameshot: flameshot is an amazing screenshot utility, the library is to work around some icon issues within the tool
# neofetch: a console application for displaying your system information (newer version of screenfetch)
# pavucontrol: a dedicated application for managing pulseaudio sinks.  Can be useful above and beyond the built in "sounds" setting tool that comes with Gnome
sudo dnf install -y flameshot neofetch pavucontrol

# MultiMedia Codes
###############################################################################
# Note this portion of the script requires that the "unofficial" RPMFusion repostories have been installed

# Packages:
# ffmpeg-libs: support for h264 codec
sudo dnf install -y ffmpeg-libs

# Update the dnf group with additional codes from the RPMFusion repositories
sudo dnf groupupdate -y Multimedia 


