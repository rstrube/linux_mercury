#!/bin/sh
# Script: Shrink Fedora Swap [Fedora]
# Author: Robert Strube
# Date: 2020-03-28

# Fedora uses LVM (logical volume management) rather than traditional partions
# See https://www.thegeekdiary.com/redhat-centos-a-beginners-guide-to-lvm-logical-volume-manager/ for more information

# With a standard installation you'll have:
# -- a FAT partion for EFI
# -- a ext4 partion for /boot
# -- an LVM2 volume group (this takes up all the rest of the space)

# The volume group contains three logical volumes:
# ---- / (ext4 fs)
# ---- /home (ext4 fs)
# ---- swap

# Fedora allocation for swap is fairly high to support hiberation.  If this is not something that is required, you can safely reduce the swap logical volume size and increase the size for /home

# Reference Commands:
# pvs: shows the size of all physical volumes
# vgs: shows the size of all volume groups
# lvs: shows the size of all logical volumes
# pvdisplay: detailed information on all physical volumes
# vgdisplay: detailed information on all volume groups
# lvdislay: detailed information on all logical volumes

# First lets disable swap entirely while we are making the adjustments
sudo swapoff /dev/fedora_localhost-live/swap

# Now lets shrink down the logical volume for swap to 4GB
sudo lvreduce -L 4G /dev/fedora_localhost-live/swap

# Remake the swap
sudo mkswap /dev/fedora_localhost-live/swap

# Re-enable swap
sudo swapon /dev/fedora_localhost-live/swap

# Extend the /home logical volume to consume all remaining space in the volume group
sudo lvextend -l +100%FREE /dev/fedora_localhost-live/home

# Resize the FS of the /home logical volume (it's safe to extend without unmounting)
sudo resize2fs /dev/fedora_localhost-live/home



