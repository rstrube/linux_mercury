#!/bin/sh
# Script: Gnome Appearance Resources [Fedora]
# Author: Robert Strube
# Date: 2020-08-10

sudo dnf install -y papirus-icon-theme papirus-folders

# Install Dracula GTK theme to ~/.themes
mkdir -p $HOME/.themes
cp -r ../cross_distro/themes/gtk/Dracula $HOME/.themes
