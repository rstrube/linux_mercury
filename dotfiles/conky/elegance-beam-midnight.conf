--[[
# Theme		: elegance-beam-noon, based from Elegance-beam conky by capn-damo (https://capn-damo.deviantart.com/art/Elegance-beam-conky-691220003)
# Author	: F Hamzah <fahrud26@gmail.com>
# License	: Distributed under the terms of GNU GPL version 2 or later
# WiFi		: To determine your wireless network device, use the command 'iw dev'
# Sensors	: To determine your hardware temperature sensor, use the command 'sensors'
# 			  and also look in /sys/devices/platform/coretemp.0/xxx for clues as to which sensor to use
]]

conky.config = {
	alignment = 'bottom_right',
	background = false,
	border_inner_margin = 15,
	border_width = 5,
	default_color = 'ffffff',  --ffffff # grey 5f5f5f 3F3F3F 183149 3B3B3B 26211F
	double_buffer = true,
	draw_borders = false,
	draw_graph_borders = false,
	draw_outline = false,
	draw_shades = false,
	gap_x = 65,
	gap_y = 0,
	maximum_width = 320,
	double_buffer = true,
	override_utf8_locale = true,
    own_window = true,
    own_window_class = 'Conky',
    own_window_type = 'normal',
	own_window_transparent = true,
	own_window_hints = 'undecorated,below,skip_taskbar,skip_pager,sticky',
	own_window_argb_visual = true,
	own_window_argb_value = 150,
	text_buffer_size = 8000,
	total_run_times = 0,
	update_interval = 1,
	uppercase = false,
	use_xft = true,
	xftalpha = 1,
	short_units = false,
	font = 'Raleway:style=Light:pixelsize=16',
	color1 = '9FA2BA',
	color2 = '74778B',
	color3 = '525461',
};

conky.text = [[
${font Raleway:pixelsize=150}${alignr}${color1}${time %H}
${voffset -35}${alignr}${color2}${time %M}
${voffset -90}${color3}${font Raleway:style=Medium:pixelsize=25}${alignr}${time %A}  |  ${alignr}${time %B %d, %Y}${color}${font}
${voffset 20}cpu freq ${alignr}${freq}
cpu usage ${alignr}${cpu}%
cpu temp ${alignr}${hwmon 2 temp 1}°C
ram ${alignr}${mem} / ${memmax}
swap ${alignr}${swap} / ${swapmax}
system ${alignr}${fs_used_perc /}% / ${fs_size /}
${voffset 20}${font Raleway:style=Medium:pixelsize=18}${alignr}${color3}Networking${color}${font}
${voffset 20}wifi ${alignr}up: ${upspeed wlp32s0} down: ${downspeed wlp32s0}
${voffset 20}${font Raleway:style=Medium:pixelsize=18}${alignr}${color3}Processes${color}${font}
${voffset 20}${alignr}pid/cpu/mem%
${top name 1}${alignr}${top pid 1}${top cpu 1}${top mem 1}
${top name 2}${alignr}${top pid 2}${top cpu 2}${top mem 2}
${top name 3}${alignr}${top pid 3}${top cpu 3}${top mem 3}
${top name 4}${alignr}${top pid 4}${top cpu 4}${top mem 4}
${top name 5}${alignr}${top pid 5}${top cpu 5}${top mem 5}
${voffset 20}${alignr}total: ${processes} run: ${running_processes}
${voffset 20}${font Raleway:style=Medium:pixelsize=18}${alignr}${color3}Linux ${kernel}${color}
]];
