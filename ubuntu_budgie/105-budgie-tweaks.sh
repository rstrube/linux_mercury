#!/bin/sh
# Script: Budgie Tweaks
# Author: Robert Strube
# Date: 2020-03-28

# This setups the Budgie Backports PPA (supports getting upstream Budgie changes)
# It also installs some Gnome applications that aren't included by default
# in a minimal Ubuntu Budgie installation

# Setup the "Budgie Backports" PPA to get upstream updates for Budgie DE
sudo add-apt-repository -y ppa:ubuntubudgie/backports
sudo apt upgrade -y

# Install some important gnome applications that don't come with Budgie by default
sudo apt install -y gnome-disk-utility gnome-system-monitor gnome-text-editor eog gnome-calculator

# Install a budgie applet for system monitoring (CPU, Mem, Temp, etc.)
sudo apt install -y budgie-sysmonitor-applet

